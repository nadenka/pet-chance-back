import express from "express";
import multer from "multer";
import fs from "fs";
const router = express.Router();
const upload = multer({dest: 'images/'});
router.post('/', upload.array('file'), (req, res) => {
    const files = req.files as any[];
    const namesArr: string[] = [];

    if (files?.length) {
        for (let file of files) {
            const name = 'images/' + file.filename + '.' + file.mimetype.split('/')[1];
            namesArr.push(name);
            fs.rename('images/' + file.filename, name, (err) => {
                if (err) console.log('ERROR: ' + err);
            });
        }
        res.send(JSON.stringify(namesArr));
    }
});
export default router;