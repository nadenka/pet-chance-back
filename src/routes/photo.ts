import express from "express";
import multer from "multer";
import fs from "fs";
const router = express.Router();
const upload = multer({dest: 'images/'});

router.post('/',
    upload.single('file'),
    (req, res) => {
        if (req.file) {
            const name = 'images/' + req.file.filename + '.' + req.file.mimetype.split('/')[1];
            fs.rename('images/' + req.file.filename, name, (err) => {
                if (err) console.log('ERROR: ' + err);
            });
            res.send(JSON.stringify(name));
        }
    }
);

router.post('/photos', upload.array('file'), (req, res) => {
    const files = req.files as any[];
    const namesArr: string[] = [];

    if (files?.length) {
        for (let file of files) {
            const name = 'images/' + file.filename + '.' + file.mimetype.split('/')[1];
            namesArr.push(name);
            fs.rename('images/' + file.filename, name, (err) => {
                if (err) console.log('ERROR: ' + err);
            });
        }
        res.send(JSON.stringify(namesArr));
    }
});

router.delete('/', (req, res) => {
        fs.rm(req.body.name, () => {
        });
        res.send(JSON.stringify('ok'));
    }
);

export default router;