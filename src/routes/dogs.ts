import express from "express";
import db from "../db/conn.js";
import {dogsCollection} from "../constants/constants.js";

const router = express.Router();

router.get("/", async (req, res) => {
    let collection = await db.collection(dogsCollection).find({isDeleted: false}).toArray();
    res.send(collection).status(200);
});

export default router;