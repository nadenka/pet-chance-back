import express from "express";
import db from "../db/conn.js";
import {dogsCollection} from "../constants/constants.js";
import {ObjectId} from "mongodb";
import fs from "fs";
import {Dog} from "../interfaces/dog.js";

const router = express.Router();

router.post("/", async (req, res) => {
    const collection = await db.collection(dogsCollection);
    const newDog = req.body;
    const result = await collection.insertOne({...newDog, isDeleted: false});
    res.send(result.insertedId).status(204);
});

router.delete("/:id", async (req, res) => {
    const result = await db.collection(dogsCollection).updateOne(
        {_id: new ObjectId(req.params.id)},
        {$set: {isDeleted: true}}
    )

    const dog = await db.collection(dogsCollection)
        .findOne({_id: new ObjectId(req.params.id)}) as Dog;

    if (dog.src) {
        fs.rm(dog.src, () => {
        });
    }
    if (dog.pictures?.length) {
        dog.pictures.forEach((el) => {
            fs.rm(el, () => {
            });
        })
    }
    res.send(result).status(204);
})

router.put('/:id', async (req, res) => {
    const dog = req.body as Dog;
    if (dog && !dog.isDeleted) {
        if (dog.src !== req.body.src) {
            fs.rm(dog.src, () => {
            });
        }
        if (dog.pictures.length) {
            dog.pictures.forEach((el) => {
                if (!req.body.pictures.length || !req.body.pictures.includes(el)) {
                    fs.rm(el, () => {
                    });
                }
            })
        }
        const result = await db.collection(dogsCollection).updateOne(
            {_id: new ObjectId(req.params.id)},
            {$set: dog}
        );
        res.send(result).status(204);
    } else {
        fs.rm(req.body.src, () => {
        });
        res.send("Wrong id").status(404);
    }
})

export default router;
