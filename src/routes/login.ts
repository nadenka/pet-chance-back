import express from "express";
import bcrypt from "bcryptjs";
import db from "../db/conn.js";
import {
    accessDateDifMin,
    data,
    md5,
    refreshDateDifMin,
    userCollection
} from "../constants/constants.js";
import {secret, SECRET} from "../secret/secret.js";
import {ObjectId} from "mongodb";

const router = express.Router();

router.post("/", async (req, res) => {
    const users = await db.collection(userCollection);
    try {
        const user = await users.findOne({ login: req.body.login });
        if (user) {
            const result = await bcrypt.compare(req.body.password, user.password);
            if (result) {
                const date = +new Date() + '';
                const access = md5(`${date} ${SECRET}`);
                const refreshSecret = secret();
                const refresh = `${md5(`${date} ${refreshSecret}`)}_${user._id}`;

                await db.collection(userCollection).updateOne(
                    {_id: new ObjectId(user._id)},
                    {$set: {secret: refreshSecret}}
                );

                res.send({ access, accessDate: date, refresh, refreshDate: date });
            } else {
                res.status(401).json({ error: "password doesn't match" });
            }
        } else {
            res.status(401).json({ error: "User doesn't exist" });
        }
    } catch (error) {
        res.status(401).json({ error });
    }
});

router.post("/check", async (req, res) => {
    if (req.headers.authorization) {
        const token = req.headers.authorization
        const date = req.headers["last-modified"];
        if (token && date && +date) {
            const payload = (token === md5(`${date} ${SECRET}`));
            const isCorrectDate = +new Date() <= +data(+date, accessDateDifMin);
            if (payload && isCorrectDate) {
                res.send(JSON.stringify("true")).status(204);
            } else {
                res.status(401).json({ error: "token verification failed" });
            }
        } else {
            res.status(401).json({ error: "malformed auth header" });
        }
    } else {
        res.status(401).json({ error: "No authorization header" });
    }
})

router.post("/check-refresh", async (req, res) => {
    const token = req.body.token;
    const date = req.body.date;

    if (token &&  date && +date) {
        const splitToken = token.split('_');
        const id = splitToken[splitToken.length - 1];
        const user = await db.collection(userCollection).findOne({_id: new ObjectId(id)});
        if (user && user['secret']) {
            const payload = (token === `${md5(`${date} ${user.secret}`)}_${id}`);
            const isCorrectDate = +new Date() <= +data(+date, refreshDateDifMin);
            if (payload && isCorrectDate) {
                const accessDate = +new Date() + '';
                const access = md5(`${accessDate} ${SECRET}`);
                const refreshSecret = secret();
                const refresh = `${md5(`${date} ${refreshSecret}`)}_${user._id}`;
                await db.collection(userCollection).updateOne(
                    {_id: new ObjectId(user._id)},
                    {$set: {secret: refreshSecret}}
                );
                res.send({ access, accessDate, refresh });
            } else {
                res.status(401).json({ error: "token verification failed" });
            }
        } else {
            res.status(401).json({ error: "token verification failed" });
        }
    } else {
        res.status(401).json({ error: "malformed auth header" });
    }
})

router.post("/logout", async (req, res) => {
    const token = req.body.token;
    const splitToken = token.split('_');
    const id = splitToken[splitToken.length - 1];
    const user = await db.collection(userCollection).findOne({_id: new ObjectId(id)});
    if (user && user['secret']) {
        await db.collection(userCollection).updateOne(
            {_id: new ObjectId(user._id)},
            {$set: {secret: null}}
        );
        res.send({});
    } else {
        res.status(401).json({ error: "no refresh found" });
    }
})

export default router;
