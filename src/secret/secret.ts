export function secret() {
    const length = 64;
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    while (result.length < length) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

export const SECRET = secret();
