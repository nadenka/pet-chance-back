import { MongoClient } from "mongodb";

const connectionString = 'mongodb://localhost:27017';

const client = new MongoClient(connectionString);

const conn: MongoClient = await client.connect();

const db = conn.db("dogs");

export default db;