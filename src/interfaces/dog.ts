import {ObjectId} from "mongodb";

export interface Dog {
  mark: string;
  sex: 'девочка' | 'мальчик' | '?';
  name: string;
  years: number | undefined;
  months: number | undefined;
  address: string;
  description: string;
  src: string;
  pictures: string[];
  accountData: string;
  video: string;
  _id: ObjectId | undefined;
  section: string;
  isDeleted: boolean;
}
