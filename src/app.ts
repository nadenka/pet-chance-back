import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import dog from "./routes/dog.js";
import dogs from "./routes/dogs.js";
import photo from "./routes/photo.js"
import photos from "./routes/photos.js"
import login from "./routes/login.js"
import {accessDateDifMin, data, md5} from "./constants/constants.js";
import {SECRET} from "./secret/secret.js";

const port = 3000;
const host = '127.0.0.1';
const app = express();
const exceptionUrls = ['/dogs', '/login/check-refresh', '/login', '/login/logout'];

app.use(cors());

app.use(bodyParser.json());
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    next();
})
app.use("/images", express.static('images'));

app.options('*', cors());

app.use((req, res, next) => {
    if (exceptionUrls.includes(req.url)) {
        next();
    } else if (req.headers.authorization) {
        const token = req.headers.authorization
        const date = req.headers["last-modified"];
        if (token && date && +date) {
            const payload = (token === md5(`${date} ${SECRET}`));
            const isCorrectDate = +new Date() <= +data(+date, accessDateDifMin);
            if (payload && isCorrectDate) {
                next();
            } else {
                res.status(401).json({ error: "token verification failed" });
            }
        } else {
            res.status(401).json({ error: "malformed auth header" });
        }
    } else {
        res.status(401).json({ error: "No authorization header" });
    }

});

app.use("/dog", dog);
app.use("/dogs", dogs);
app.use("/photo", photo);
app.use("/photos", photos);
app.use("/login", login);
app.listen(port, host, () => {
    console.log(`Example app listening on port ${port}`)
})
