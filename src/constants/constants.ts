import crypto from "crypto";

export const dogsCollection = 'dogs';
export const userCollection = 'user';
export const data = (date: number, diffMin: number) => new Date(new Date(date).getTime() + diffMin * 60_000);
export const accessDateDifMin = 15;
export const refreshDateDifMin = 24 * 60;
export const md5 = (contents: string) => crypto.createHash('md5').update(contents).digest("hex");
