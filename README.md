# Pet Chance Back

## Initial Set-Up

### ufw
```bash
ufw enable
ufw allow 80
ufw allow 443
ufw allow 22
ufw allow 2222
ufw deny 27017
ufw status
```

### nginx
```nginx
    location /api/ {
        # try_files $uri $uri/ =404;
        proxy_pass "http://localhost:3000/";
        # proxy_set_header Host $host;
        # proxy_set_header X-Real-IP $remote_addr;
        # proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        # proxy_set_header X-Forwarded-Proto $scheme;
        # proxy_set_header X-Forwarded-Proto https;
    }
    location / {
        # try_files $uri $uri/ =404;
        proxy_pass "http://localhost:4200";
        # proxy_set_header X-Forwarded-Proto https;
    }
```

### node versions
```
nvm install lts/iron
nvm use lts/iron
```

### start mongo, create user
To create a bcrypt password: https://bcrypt.online/
```bash
$ docker run --name mongo --rm -p 27017:27017 -d mongo:8.0.3
$ docker run --name mongo --rm -p 127.0.0.1:27017:27017 -d mongo:8.0.3

$ mongosh
use dogs
db.user.createIndex({"login": 1})
db.user.insertOne({
  "login": "admin",
  "password": "$2y$10$xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  //, "plaintext_password": "pwd-here"
})
```

**Make sure** that mongo is unaccessible from outside! `$ telnet HOST 27017`

## Business as Usual
Assuming:
```bash 
$ cat ~/mongo-bckp/do_bckp.sh 
#!/bin/bash

function do_bckp {
  BACKUP_DIR=$(date +"%Y%m%d-%H%M")
  if [ -d "$BACKUP_DIR" ]
  then
    echo "Backup directory $BACKUP_DIR already exists. Skipping backup."
    return 0  # Exit the function early
  fi
  mkdir -p "$BACKUP_DIR"
  $HOME/mongodb-tools/bin/mongodump --uri="mongodb://localhost:27017" --db=dogs --out="$BACKUP_DIR"
  echo "Backup completed at $(date)"
}

while true
do
  do_bckp
  # 8 hours * 60 minutes * 60 seconds
  sleep $((8*60*60))
done

# TO RESTORE
# $HOME/mongodb-tools/bin/mongorestore --uri="mongodb://localhost:27017" ~/mongo-bckp/20240000-0000/
```

### tmux windows
1. front-end
    1. `exec su - petter`
    1. `cd ~/repo/pet-chance-main`
    1. `npm i`
    1. `npm run start`
1. back-end
    1. `exec su - petter`
    1. `cd ~/repo/pet-chance-back-main`
    1. `npm i`
    1. `npm run start`
1. backup
    1. `cd ~/mongo-bckp/`
    1. `bash do_bckp.sh`
